<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Password;
use App\Entity\Category;
use App\Entity\Tag;


class PasswordFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {   
        $categoArray = [];
        $tagArray = [];

        for ($i = 1; $i < 30; $i++) {

            $tag = new Tag();
            $tag->setName("name $i");
            $tagArray[] = $tag;

            $manager->persist($tag);
        }

        for ($i = 1; $i < 10; $i++) {

            $category = new Category();
            $category->setName("name $i");
            $category->setLft($i);
            $category->setRgt($i);
            $category->setLevel($i);

            $categoArray[] = $category;
                       
            $manager->persist($category);
        }

        for ($i = 1; $i < 30; $i++) {

            $password = new Password();
            $password->setUsername("username $i");
            $password->setUrl("url $i");
            $password->setValue("value $i");
            $password->setEmail("email $i");
            $password->setName("name $i");
            $password->setCategory($categoArray[rand(1, 8)]);
            $password->getTags($tagArray[rand(1, 29)]);
            
            $manager->persist($password);
        }

        $manager->flush();
    }
}
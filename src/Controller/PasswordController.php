<?php

/*GET /passwords  OK!
GET /passwords/{id} OK!
GET /passwords?category={id}
GET /categories
POST /password-proposals
POST /passwords
PUT /passwords/{id}
DELETE /passwords/{id}
POST /categories
PUT /categories/{id}
DELETE /categories/{id}*/

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Password;
use App\Entity\Category;
use App\Entity\Tag;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/password", name="password")
 */
class PasswordController extends Controller
{
    private $serializer;
    //On injecte le serializer dans le constructeur de la classe
    //contrôleur car on devra s'en servir dans toutes les méthodes
    //de celle ci.
    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", methods={"GET"})
     */
    public function index()
    {
        $repo = $this->getDoctrine()->getRepository(Password::class);
        
        //On utilise le serializer de symfony pour transfomer une
        //entité (ou array d'entités) php au format json
        $json = $this->serializer->serialize($repo->findAll(), "json");
        //On utilise la méthode static fromJsonString de la classe
        //JsonResponse pour créer une réponse HTTP en json à partir
        //de données déjà au format JSON
        return JsonResponse::fromJsonString($json);
    }
    
    /**
     * @Route("/", methods={"POST"})
     */
    public function add(Request $request) {
        //On récupère le body de la request (les données de celle ci)
        $body = $request->getContent();
        //On transforme ce body en instance de password depuis le format json
        $password = $this->serializer->deserialize($body, Password::class, "json");
        //On fait persister le password via doctrine
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($password);
        $manager->flush();
        //On envoie une réponse pour dire que c'est ok et dire l'id qui vient d'être ajouté
        return new JsonResponse(["id" => $password->getId()]);
    }

    /**
     * @Route("/{password}", methods={"GET"})
     */
    public function single(password $password) {
        $json = $this->serializer->serialize($password,"json");
        return JsonResponse::fromJsonString($json);
    }


    /**
     * @Route("/password?category={id}", methods={"GET"})
     */
    public function getCatego(category $category) {
        $json = $this->serializer->serialize($category,"json");
        return JsonResponse::fromJsonString($json);
    }


    /**
     * @Route("/{password}", methods={"DELETE"})
     */
    public function remove(password $password) {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($password);
        $manager->flush();
        return new Response("", 204);
    }

    /**
     * @Route("/{password}", methods={"PUT"})
     */
    public function update(password $password, Request $request) {
        $body = $request->getContent();
        $updated = $this->serializer->deserialize($body, Password::class, "json");

        $manager = $this->getDoctrine()->getManager();

        $password->setName($updated->getName());
        $password->setSurname($updated->getSurname());
        $password->setLevel($updated->getLevel());
        $password->setTech($updated->getTech());
        
        $manager->flush();
        return new Response("", 204);
    }

    
}